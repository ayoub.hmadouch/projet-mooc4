import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Product } from '../models/product.models';
import { Observable, ObservedValueOf } from 'rxjs';


@Injectable({providedIn: 'root'})
export class ProductsService {

    private host = environment.host;

    constructor(private httpClient: HttpClient) { }

    getAllProducts() : Observable<Product[]> {
        return this.httpClient.get<Product[]>(this.host + "/products");
    }
    getSelectedProducts() : Observable<Product[]> {
        return this.httpClient.get<Product[]>(this.host + "/products?selected=true");
    }
    getAvailableProducts() : Observable<Product[]> {
        return this.httpClient.get<Product[]>(this.host + "/products?available=true");
    }

    search(keyword:string): Observable<Product[]> {
        return this.httpClient.get<Product[]>(this.host + "/products?name_like=" + keyword);
    }

    select(p: Product): Observable<Product>{
        p.selected = !p.selected;
        return this.httpClient.put<Product>(this.host + "/products/" + p.id, p);
    }
    delete(p: Product): Observable<void>{
        return this.httpClient.delete<void>(this.host + "/products/" + p.id);
    }

    save(p: Product): Observable<Product>{
        return this.httpClient.post<Product>(this.host + "/products/" , p);
    }

    getProduct(id: number): Observable<Product>{
        return this.httpClient.get<Product>(this.host + "/products/" + id);
    }

    updateProduct(product: Product): Observable<Product>{
        return this.httpClient.put<Product>(this.host +"/products/"+product.id,product)
    }
}