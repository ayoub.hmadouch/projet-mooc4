import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product.models';
import { ProductsService } from 'src/app/services/products.service';
import { catchError, map, Observable, of, startWith } from 'rxjs';
import { appDataState, dataStateEnum } from 'src/state/products.state';
import { Router } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
	products$: Observable<appDataState<Product[]>> | null = null;
	readonly dataStateEnum = dataStateEnum;
	constructor(
	private productsService: ProductsService,
	private router :Router
	) { }

	ngOnInit(): void {
	}

	getAllProducts() {
	this.products$ = this.productsService.getAllProducts().pipe(
		map((data) => ({ dataState: dataStateEnum.LOADED, data: data })),
		startWith({ dataState: dataStateEnum.LOADING }),
		catchError(err => of ({dataState:dataStateEnum.ERROR,errorMessage : err.message}))
	);
	}

	getSelectedProducts() {
	this.products$ = this.productsService.getSelectedProducts().pipe(
		map((data) => ({ dataState: dataStateEnum.LOADED, data: data })),
		startWith({ dataState: dataStateEnum.LOADING }),
		catchError(err => of({ dataState: dataStateEnum.ERROR, errorMessage: err.message }))
	);
	}

	getAvailableProducts() {
	this.products$ = this.productsService.getAvailableProducts().pipe(
		map((data) => ({ dataState: dataStateEnum.LOADED, data: data })),
		startWith({ dataState: dataStateEnum.LOADING }),
		catchError(err => of ({dataState:dataStateEnum.ERROR,errorMessage : err.message}))
	);
	}

	onSearch(dataForm: any) {
	this.products$ = this.productsService.search(dataForm.keyword).pipe(
		map((data) => ({ dataState: dataStateEnum.LOADED, data: data })),
		startWith({ dataState: dataStateEnum.LOADING }),
		catchError(err => of ({dataState:dataStateEnum.ERROR,errorMessage : err.message}))
	);
	}

	onSelected(p: Product) {
	this.productsService.select(p).subscribe(
		(data) => {
			p.selected = data.selected;
		}
	);

	}
	onDelete(p: Product) {
	let v = confirm("Confirmez-vous la suppression de ce produit?");
		if (v == true) {
			this.productsService.delete(p).subscribe(
			() => {
			this.getAllProducts();
			})
		}
	}
	onEdit(p: Product) {
		this.router.navigateByUrl("/editProduct/" + p.id);
	}

}
