import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductsService } from 'src/app/services/products.service';


@Component({
	selector: 'app-products-form',
  	templateUrl: './products-form.component.html',
  	styleUrls: ['./products-form.component.css']
})
export class ProductsFormComponent implements OnInit {

	productFormGroup: FormGroup = new FormGroup({});
	submitted: boolean = false;
	constructor(
		private fb: FormBuilder,
		private productService : ProductsService
	) { }

	ngOnInit(): void {
		this.productFormGroup = this.fb.group({
			name: ["", [Validators.required,Validators.minLength(3)]],
			price: [0, Validators.required],
			quantity: [0, Validators.required],
			selected: [true, Validators.required],
			available : [true ,Validators.required]
		})

	}

	onSaveProduct(): void {
		this.submitted = true;
		if (this.productFormGroup.invalid) return;
		this.productService.save(this.productFormGroup.value)
			.subscribe(data => {
				alert("success");
			});
	}


}
