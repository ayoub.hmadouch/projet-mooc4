export enum dataStateEnum{
    LOADING,
    LOADED,
    ERROR
}

export interface appDataState<T>{
    dataState?:dataStateEnum,
    data?: T,
    errorMessage?:string
}